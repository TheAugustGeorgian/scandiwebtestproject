<?php
	include_once "helpers/dbh.php";
	include_once "helpers/classes.php";
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		ProductFactory::deleteProducts($conn, $_POST['skus']);
		$conn->close();
		header("Location: /");
		die();
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="static/styles.css">
		<title>Product List</title>
	</head>
	<body>
		<header class="navbar">
			<h1>Product List</h1>
			<div class="buttons">
				<button onclick="window.location.href='addproduct.php';">ADD</button>
				<button id="delete-product-btn" type="submit">MASS DELETE</button>
			</div>
		</header>
		
		<form action="/" method="POST">
			<div id="products">
				<?php
					$products = ProductFactory::getProducts($conn);
					foreach ($products as $product) {
						echo "<div class='product'>
								<input class='delete-checkbox' type='checkbox' id='{$product->getSKU()}' name='skus[]' value='{$product->getSKU()}'>
								<div class='product-data'>
									<p>{$product->getSKU()}</p>
									<p>{$product->getName()}</p>
									<p>{$product->getPrice()}</p>
									<p>{$product->getAdditionalInfo()}</p>
								</div>
							</div>";
					}
					
				?>
			</div>
			<input id="delete" type="submit" style="visibility: hidden;">
		</form>
		<script>
			let delete_btn = document.getElementById('delete-product-btn');
			delete_btn.addEventListener('click', function(){
			    let checkboxes = document.getElementsByClassName("delete-checkbox");
			    for (let i = 0; i < checkboxes.length; i++){
			        if (checkboxes[i].checked){
			           document.getElementById('delete').click();
			           break;
			        }
			    }
			});
		</script>
	</body>
</html>
