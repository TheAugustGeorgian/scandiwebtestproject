<?php 
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		include_once("helpers/dbh.php");
		include_once("helpers/classes.php");

    	$sku = mysqli_real_escape_string($conn, $_POST['sku']);
		$name = mysqli_real_escape_string($conn, $_POST['name']);
		$price = $_POST['price'];
		$type = $_POST['productType'];
		$additionalParams = $_POST['additional-params'];
		$additionalParam = ($type == 2)? "{$additionalParams[0]}x{$additionalParams[1]}x{$additionalParams[2]}" : $additionalParams[0];

		$product = ProductFactory::createProduct($sku, $name, $price, $type, $additionalParam);
		$product->insertIntoDB($conn);
		$conn->close();
    	header("Location: /");
		die();
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="static/styles.css">
		<title>Product Add</title>
	</head>
	<body>
		<header class="navbar">
			<h1>Product Add</h1>
			<div class="buttons">
				<button id="save" type="submit">Save</button>
				<button onclick="window.location.href='/';">Cancel</button>
			</div>
		</header>
		<form id="product_form" action="addproduct.php" method="POST">
			<div class="item">
				<label for="sku">SKU
					<input id="sku" type="text" name="sku" maxlength="10" required>
				</label>
			</div>
			<div class="item">
				<label for="name">Name
					<input id="name" type="text" name="name" maxlength="30" required>
				</label>
			</div>
			<div class="item">
				<label for="price">Price ($)
					<input id="price" type="number" name="price" min=0 required>
				</label>
			</div>
			<div class="item">
				<label for="productType">Type Switcher
					<select id="productType" name="productType" required>
					 <option id="typeSwitcher" selected disabled>Type Switcher</option> 
					  <option id="DVD" value="1">DVD</option>
					  <option id="Furniture" value="2">Furniture</option>
					  <option id="Book" value="3">Book</option>
					</select>
				</label>
			</div>
			<small id="error" style="visibility:hidden">please, select a product type</small>
			<div id="additional-options"></div>
			<input id="add" type="submit" style="visibility: hidden;">
		</form>
	</body>
	<script>
		let save = document.getElementById('save');
		save.addEventListener('click', function(){
			if (document.getElementById("typeSwitcher").selected){
				document.getElementById('error').style.visibility="visible";
			} else {
				document.getElementById('add').click();
			}
			
		});
		
		let DVDOptions = "<div class='item'>\
						   <label for='size'>Size (MB)\
						    <input id='size' type='number' min='0' name='additional-params[]' required>\
						   </label>\
					 	  </div>\
					 	  <small>Please, provide size</small>";
		let furnitureOptions = "<div class='item'>\
						   <label for='height'>Height (CM)\
						    <input id='height' type='number' min='0' name='additional-params[]' required>\
						   </label>\
					 	  </div>\
					 	  <div class='item'>\
						   <label for='width'>Width (CM)\
						    <input id='width' type='number' min='0' name='additional-params[]' required>\
						   </label>\
					 	  </div>\
					 	  <div class='item'>\
						   <label for='length'>Length (CM)\
						    <input id='length' type='number' min='0' name='additional-params[]' required>\
						   </label>\
					 	  </div>\
					 	  <small>Please, provide dimensions</small>";
		let bookOptions = "<div class='item'>\
						   <label for='weight'>Weight (KG)\
						    <input id='weight' type='number' min='0' name='additional-params[]' required>\
						   </label>\
					 	  </div>\
					 	  <small>Please, provide weight</small>";
		
		let additionalOptions = document.getElementById('additional-options')
		document.getElementById('productType').addEventListener("change", function() {
			document.getElementById('error').style.visibility="hidden";
			if (document.getElementById("DVD").selected) {
				additionalOptions.innerHTML = DVDOptions;
			}
			else if (document.getElementById("Furniture").selected){
				additionalOptions.innerHTML = furnitureOptions;
			}
			else if (document.getElementById("Book").selected){
				additionalOptions.innerHTML = bookOptions;
			}
		});
		
	</script>
</html>
