<?php
	
	abstract class Product {
		private $sku;
		private $name;
		private $price;
		private $type;
	
	
		abstract protected function getAdditionalInfo();
		abstract protected function setAdditionalInfo($newValue);
		
		public function __construct($sku, $name, $price, $type){
			$this->sku = $sku;
			$this->name = $name;
			$this->price = $price;
			$this->type = $type;
		}
		
		public function getSKU(){
			return $this->sku;
		}
		
		public function getName(){
			return $this->name;
		}
		
		public function getPrice(){
			return "{$this->price} $";
		}
		
		public function getType(){
			return $this->type;
		}
		
		public function setSKU($newSKU){
			$this->sku = $newSKU;
		}
		
		public function setName($newName){
			$this->name = $newName;
		}
		
		public function setPrice($newPrice){
			$this->price = $newPrice;
		}
		
		protected function insertIntoDB($conn, $additionalParam=null){
			$sql = "INSERT INTO products VALUES ('{$this->sku}', 
												'{$this->name}',
												{$this->price},
												{$this->type},
												'{$additionalParam}')";
		
			$conn->query($sql);
		}
	}




	class DVD extends Product{
		private $size;
		
		public function __construct($sku, $name, $price, $type, $additionalParam) {
		    parent::__construct($sku, $name, $price, $type);
		    $this->size = $additionalParam;
		}
		
		public function getAdditionalInfo(){
			return "Size: {$this->size} MB";
		}
		
		public function setAdditionalInfo($newValue){
			$this->size = $newValue;
		}
		public function insertIntoDB($conn, $additionalParam=null){
			parent::insertIntoDB($conn, $this->size);
		}
	}
	
	
	
	class Furniture extends Product{
		private $dimensions;
		
		public function __construct($sku, $name, $price, $type, $additionalParam) {
		    parent::__construct($sku, $name, $price, $type);
		    $this->dimensions = $additionalParam;
		}
		
		public function getAdditionalInfo(){
			return "Dimension: {$this->dimensions}";
		}
		
		public function setAdditionalInfo($newValue){
			$dimensions = $newValue;
		}
		
		public function insertIntoDB($conn, $additionalParam=null){
			parent::insertIntoDB($conn, $this->dimensions);
		}
		
	}
	
	
	class Book extends Product{
		private $weight;
		
		public function __construct($sku, $name, $price, $type, $additionalParam) {
		    parent::__construct($sku, $name, $price, $type);
		    $this->weight = $additionalParam;
		}
		
		public function getAdditionalInfo(){
			return "Weight: {$this->weight}KG";
		}
		
		public function setAdditionalInfo($newValue){
			$this->weight = $newValue;
		}
		
		public function insertIntoDB($conn, $additionalParam=null){
			parent::insertIntoDB($conn, $this->weight);
		}

	}
	
	
	class ProductFactory {
	
		public static function createProduct($sku, $name, $price, $type, $additionalParam){
			$creatorDict = [1 => new DVD($sku, $name, $price, $type, $additionalParam),
							2 => new Furniture($sku, $name, $price, $type, $additionalParam),
							3 => new Book($sku, $name, $price, $type, $additionalParam)];
							
			return $creatorDict[$type];
		}
		
		public static function getProducts($conn){
			$result = $conn->query("SELECT * FROM products ORDER BY sku;");
			$products = [];
			while ($row = mysqli_fetch_assoc($result)){
				array_push($products, self::createProduct($row['sku'], $row['name'], $row['price'], $row['type'], $row['unique_param']));
			}
			return $products;
		}
		
		public static function deleteProducts($conn, $skus){
			$decoratedSkus = [];
			foreach($skus as $sku){
				array_push($decoratedSkus, "'{$sku}'");
			}
			$skusText = implode(', ', $decoratedSkus);
			$sql = "DELETE FROM products WHERE sku IN ({$skusText});";
			$conn->query($sql);
		}
	}
	
